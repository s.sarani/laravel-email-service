<?php

namespace App\Repositories;

use App\Models\Email;
use Carbon\Carbon;
use Illuminate\Http\Request;


class EmailRepository
{
    protected Email $email;

    public function __construct()
    {
        $this->email = new Email();
    }


    public function insertEmail($request)
    {
        $request['status'] = Email::STATUS_PENDENG;
        return Email::create(

            $request
        );

    }


    public function insertBulkEmail($request)
    {
        $request['status'] = Email::STATUS_PENDENG;
        return Email::create(
            $request
        );

    }


    public function updateEmail($request)
    {
            Email::where('id', '=', $request)
                ->update([
                    'status' => Email::STATUS_DONE,
                    'sent_at' => now()
                ]);
    }


    public function updateEmailFailed($request)
    {
            Email::where('id', '=', $request)
                ->update([
                    'status' => Email::STATUS_FAILED,
                ]);
    }


    public function getEmailPending()
    {
        return Email::where('status', '=', Email::STATUS_PENDENG)
            ->limit(5)
            ->get();
    }


    public function getEmail()
    {
        return Email::paginate(4);
    }

    public function get($key , $value)
    {
        return Email::where($key ,'=' ,$value)
            ->where('status','=',Email::STATUS_PENDENG)
            ->limit(Email::LIMIT)
            ->get();
    }
}
