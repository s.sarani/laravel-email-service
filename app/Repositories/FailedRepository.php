<?php

namespace App\Repositories;

use App\Models\Fail;

class FailedRepository
{
    protected Fail $fail;

    public function __construct()
    {
        $this->fail=new Fail();
    }

    public function create($id,$type)
    {
        $this->fail->insert([
            'email_id'=>$id,
            'Message_type'=>$type,
            'failed_at'=>now()
        ]);
    }

    public function count($id)
    {
        return Fail::where('email_id','=' ,$id)->count();
    }

    public function createFailed($countFail, $item ,$type , EmailRepository $emailRepository): void
    {
        if ($countFail < Fail::COUNT_FAILED) {
            $this->create($item,$type);
        }
        if ($countFail==Fail::COUNT_FAILED)
        {
            $emailRepository->updateEmailFailed($item);
        }
    }
}
