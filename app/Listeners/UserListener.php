<?php

namespace App\Listeners;

use App\Events\UserEvents;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserEvents  $event
     * @return void
//     */
    public function handle(UserEvents $event)
    {
      echo  $event->getFamily(). "<br>";

    }
}
