<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fail extends Model
{
    use HasFactory;
    protected $table='failed_email';
    const COUNT_FAILED=3;
    protected $guarded=[];
}
