<?php

namespace App\Models;

use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;
    const STATUS_PENDENG = 'pending';
    const STATUS_DONE = 'done';
    const STATUS_FAILED = 'failed';
    const STATUS_CANCELED = 'canceled';
    const TYPE_WELCOME='welcome';
    const TYPE_INVOICE='invoice';
    const TYPE_LOGIN='login';
    const LIMIT=10;

    protected $fillable = [
        'email',
        'subject',
        'type',
        'status',
        'sent_at',
    ];

    public function setEmailAttribute($value)
    {
        is_array($value)?$this->attributes['email']=json_encode($value):$this->attributes['email']= $value ;
    }

    public function getSentAtAttribute($value)
    {
        $verta=new Verta($value);
       return $verta->format('Y-m-d H:i:s');
    }


}
