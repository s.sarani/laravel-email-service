<?php

namespace App\Console\Commands;

use App\Mail\WelocmeMail;
use App\Models\Fail;
use App\Repositories\EmailRepository;
use App\Repositories\FailedRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Exception;

class WelcomeSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:welcome';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command For sent Email Welcome';
    protected $email;
    protected  $fail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->email = new EmailRepository();
        $this->fail=new FailedRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->welcomeMail();
        return 0;
    }
    public function welcomeMail(): void
    {
        $login = $this->email->get('type', 'welcome');
        $login->each(function ($item) {
            try {
                Mail::to($item)->send(new WelocmeMail($item, $item->id));
                $this->email->updateEmail($item->id);
                echo "ID:" . $item->id . ' ' . "Email:" . $item->email . PHP_EOL;
            }catch(\Exception $e){
                $countFail=$this->fail->count($item->id);
                $this->fail->createFailed($countFail, $item->id , $item->type,$this->email);
            }

        });
    }



}
