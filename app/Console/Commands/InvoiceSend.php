<?php

namespace App\Console\Commands;

use App\Mail\InvoiceMail;
use App\Mail\LoginMail;
use App\Mail\WelocmeMail;
use App\Models\Email;
use App\Repositories\EmailRepository;
use App\Repositories\FailedRepository;
use App\Service\EmailService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Exception;

class InvoiceSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:invoice';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'This Command For Send Invoice Emails';

    protected $emailSend;
    protected $email;
    protected $fail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->email = new EmailRepository();
        $this->fail = new FailedRepository();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->invoiceMail();
    }


    /**
     * @return void
     */


    public function invoiceMail(): void
    {
        $login = $this->email->get('type', 'invoice');
        $login->each(function ($item) {
            try {
                Mail::to($item)->send(new InvoiceMail($item, $item->id));
                $this->email->updateEmail($item->id);
                echo "ID:" . $item->id . ' ' . "Email:" . $item->email . PHP_EOL;
            } catch (\Exception $e) {
                $countFail = $this->fail->count($item->id);
                d($countFail);
                $this->fail->createFailed($countFail, $item->id, $item->type, $this->email);
            }

        });


    }


}

