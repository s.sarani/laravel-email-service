<?php

namespace App\Http\Controllers;

use App\Events\UserEvents;
use App\Models\Test;
use App\Repositories\EmailRepository;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class TestController extends Controller
{
    private $emailRepository;
    public function __construct()
    {
        $this->emailRepository=new EmailRepository();
    }

    public function store(Request $request)
    {
        $data=$request->validate([
           'email'=>'required|email',
           'subject'=>'required|string',
           'body'=>'required|string',
        ]);

        return response()->json($this->emailRepository->insertEmail($data));
    }

    public function bulkStore(Request $request)
    {
        $data=$request->validate([
          'email'=>'required|array',
           'subject'=>'required|string',
           'body'=>'required|string',
        ]);

        return response()->json($this->emailRepository->insertBulkEmail($data));
    }

    public function getEmail(Request $request)
    {
        return $this->emailRepository->getEmail();
    }


}
