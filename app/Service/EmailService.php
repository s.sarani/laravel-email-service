<?php

namespace App\Service;

use App\Console\Commands\InvoiceSend;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer\PHPMailer;

class EmailService
{
    private $mail;
    public $sendEmail;

    public function __construct()
    {
        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        $this->mail->Host = 'smtp.mailtrap.io';
        $this->mail->SMTPAuth = true;
        $this->mail->Port = 2525;
        $this->mail->Username = '3b57e998b6516b';
        $this->mail->Password = 'c75b88783567d6';
        $this->mail->setFrom('smtp.mailtrap.io', 'Mailer');
    }

    public function SendingEmail($request)
    {
        $this->mail->addAddress($request->email);
        $this->mail->isHTML(true);
        $this->mail->Subject = $request->subject;
        $this->mail->Body = $request->body;
        return $this->mail->send();

    }
}

