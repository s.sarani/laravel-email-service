<?php

use App\Events\UserEvents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','mail.InvoiceMail');
Route::view('/login','mail.LoginMail');
Route::view('/welcome','mail.WelcomeMail');
Route::get('/get',[TestController::class , 'getEmail']);

