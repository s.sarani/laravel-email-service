<?php

namespace Database\Seeders;

use App\Models\Email;
use Database\Factories\EmailFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Email::factory()->count(3)->create();
    }
}
