<?php

namespace Database\Seeders;

use Database\Factories\EmailFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmailSeeder::class
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
