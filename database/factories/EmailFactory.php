<?php

namespace Database\Factories;

use App\Models\Email;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Email>
 */
class EmailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'email'=>$this->faker->email(),
            'subject'=>$this->faker->sentence(15),
            'type'=>$this->faker->randomElement([Email::TYPE_LOGIN,Email::TYPE_INVOICE,Email::TYPE_WELCOME]),
            'status'=>$this->faker->randomElement([Email::STATUS_PENDENG]),
        ];
    }
}
//Email::STATUS_DONE,Email::STATUS_FAILED,Email::STATUS_CANCELED
